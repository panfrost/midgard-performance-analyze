/*
 * Copyright (C) 2019 Alyssa Rosenzweig
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published
 * by the Free Software Foundation.
 */

#include <stdio.h>
#include <stdint.h>
#include <mali_counter_names.h>

/* Defines adapted from the kernel */
typedef uint32_t counter_t;
#define COUNTERS_PER_BLOCK 64

static void
decode_counter_block(uint8_t *buffer, unsigned block_index, const char * const* names, unsigned name_block_index)
{
	/* Index the block given it's size */
	size_t block_size = sizeof(counter_t) * COUNTERS_PER_BLOCK;
	uint8_t *block = buffer + (block_size * block_index);
	counter_t *counters = (counter_t *) block;

	/* Find the corresponding block of names */
	const char * const* names_block = names + (COUNTERS_PER_BLOCK * name_block_index);

	/* Dump the block */

	for (unsigned c = 0; c < COUNTERS_PER_BLOCK; ++c) {
		printf("%s: %u\n", names_block[c], counters[c]);
	}
}

/* Define order of names, at least for t86x counters */

enum name_order {
	JM = 0,
	TILER = 1,
	SHADER = 2,
	MMU = 3
};

void
decode_rk3399(uint8_t *buffer)
{
	/* Hard-code the layout for now, TODO detect */
	const char * const* counter_names = hardware_counters_mali_t86x;

	/* Job Manager */
	decode_counter_block(buffer, 0, counter_names, JM);

	/* Tiler */
	decode_counter_block(buffer, 1, counter_names, TILER);

	/* MMU */
	decode_counter_block(buffer, 2, counter_names, MMU);
	decode_counter_block(buffer, 3, counter_names, MMU);
	decode_counter_block(buffer, 4, counter_names, MMU);

	/* Shader */
	decode_counter_block(buffer, 5, counter_names, SHADER);
}

int
main(int argc, char **argv)
{
	if (argc < 2) {
		fprintf(stderr, "usage: mdgprf-analyze [file.mdgprf]\n");
		return 1;
	}

	uint8_t counter_buffer[0x100 * 6];
	const char *fn = argv[1];

	FILE *fp = fopen(fn, "rb");
	fread(counter_buffer, 1, sizeof(counter_buffer), fp);
	fclose(fp);

	decode_rk3399(counter_buffer);
}
