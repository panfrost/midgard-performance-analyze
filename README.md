Standalone tool to analyze Midgard performance counters dumped by Panfrost.

Based on GPLv2 code from Arm's gator daemon.
